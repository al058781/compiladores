
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpresionesRegulares {

    public static boolean busca_3a(String cadena) {
        String expR = ".[aA].[aA].[aA]";
        Pattern pat = Pattern.compile(expR);
        Matcher mat = pat.matcher(cadena);
        return mat.matches();
    }

    public static boolean validaNumeroEnteroPositivo_Exp(String texto) {
        return texto.matches("^[0-9]+$");
    }

    public static boolean validaNumeroEnteroNegativo_Exp(String texto) {
        return texto.matches("^-[0-9]+$");
    }

    public static boolean validaNumeroEntero_Exp(String texto) {
        return texto.matches("^-?[0-9]+$");
    }

    public static boolean validarMatriculaEuropea_Exp(String matricula) {
        return matricula.matches("^[0-9]{4}[A-Z]{3}$");
    }

    public static boolean validarMatriculaUac_Exp(String matricula) {
        return matricula.matches("^al0[0-9]{5}$");
    }

    public static boolean validaBinario_Exp(String binario) {
        return binario.matches("^[0-1]+$");
    }

    public static boolean validarUsuarioTwitter_Exp(String usuario_twitter) {
        return usuario_twitter.matches("^@([A-Za-z0-9_ -.]{1,15})$");
    }

    public static void main(String[] args) throws Exception {

        //Letras
        System.out.println(".[aA].[aA].[aA]" + "\n");
        System.out.println("mañana " + busca_3a("mañana"));
        System.out.println("banana " + busca_3a("banana"));
        System.out.println("canada " + busca_3a("canada"));
        System.out.println("campeche " + busca_3a("campeche"));
        System.out.println("campana " + busca_3a("campana"));
        System.out.println("Esto es " + busca_3a("Esto es "));

        //Fecha con números
        String regexp = "\\d{1,2}/\\d{1,2}/\\d{4}";
        System.out.println("\n" + regexp + "\n");
        // Lo siguiente devuelve true
        System.out.println("11/12/2014 " + Pattern.matches(regexp, "11/12/2014"));
        System.out.println("1/12/2014 " + Pattern.matches(regexp, "1/12/2014"));
        System.out.println("11/2/2014 " + Pattern.matches(regexp, "11/2/2014"));

        System.out.println("11/12/14 " + Pattern.matches(regexp, "11/12/14"));
        System.out.println("11//2014 " + Pattern.matches(regexp, "11//2014"));
        System.out.println("11/12/14perico " + Pattern.matches(regexp, "11/12/14perico"));

        //Fecha con meses
        String literalMonthRegexp = "\\d{1,2}/(?i)(ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|dic)/\\d{4}";
        System.out.println("\n" + literalMonthRegexp + "\n");

        System.out.println("11/dic/2014 " + Pattern.matches(literalMonthRegexp, "11/dic/2014"));
        System.out.println("1/nov/2014 " + Pattern.matches(literalMonthRegexp, "1/nov/2014"));
        System.out.println("1/AGO/2014 " + Pattern.matches(literalMonthRegexp, "1/AGO/2014"));
        System.out.println("21/Oct/2014 " + Pattern.matches(literalMonthRegexp, "21/Oct/2014"));

        System.out.println("11/abc/2014 " + Pattern.matches(literalMonthRegexp, "11/abc/2014"));
        System.out.println("11//2014 " + Pattern.matches(literalMonthRegexp, "11//2014"));
        System.out.println("11/jul/2014perico " + Pattern.matches(literalMonthRegexp, "11/jul/2014perico"));

        //Números enteros positivos
        System.out.println("\n^[0-9]+$");
        System.out.println("10 " + validaNumeroEnteroPositivo_Exp("10"));
        System.out.println("-10 " + validaNumeroEnteroPositivo_Exp("-10"));

        //Números enteros negativos
        System.out.println("\n^-[0-9]+$");
        System.out.println("-11" + validaNumeroEnteroNegativo_Exp("-11"));
        System.out.println("11 " + validaNumeroEnteroNegativo_Exp("11"));

        //Números enteros
        System.out.println("\n^-?[0-9]+$");
        System.out.println("13 " + validaNumeroEntero_Exp("13"));
        System.out.println("1.2 " + validaNumeroEntero_Exp("1.2"));

        //Matricula Uac Y Europa
        System.out.println("\n^[0-9]{4}[A-Z]{3}$");
        System.out.println("9999AAA " + validarMatriculaEuropea_Exp("9999AAA"));
        System.out.println("\n^al0[0-9]{5}$");
        System.out.println("al058781 " + validarMatriculaUac_Exp("al058781"));

        //Binario
        System.out.println("\n^[0-1]+$");
        System.out.println("0 " + validaBinario_Exp("0"));
        System.out.println("2 " + validaBinario_Exp("2"));

        // twitter
        System.out.println("\n^@([A-Za-z0-9_ -.]{1,15})$");
        System.out.println("@Henrri_mg98 " + validarUsuarioTwitter_Exp("@Henrri_mg98"));
        System.out.println("Henrri,e1 " + validarUsuarioTwitter_Exp("Henrri,e1"));

        String dniRegexp = "\\d{8}[A-HJ-NP-TV-Z]";
        System.out.println("\n" + dniRegexp);
        System.out.println(Pattern.matches(dniRegexp, "01234567C"));
        System.out.println(Pattern.matches("01234567U " + dniRegexp, "01234567U"));
        System.out.println(Pattern.matches("0123567X " + dniRegexp, "0123567X"));

        String emailRegexp = "[^@]+@[^@]+\\.[a-zA-Z]{2,}";
        System.out.println("\n"+emailRegexp);
        System.out.println("a@b.com "+Pattern.matches(emailRegexp, "a@b.com"));
        System.out.println("+++@+++.com "+Pattern.matches(emailRegexp, "+++@+++.com"));

        System.out.println("@b.com "+Pattern.matches(emailRegexp, "@b.com")); 
        System.out.println("a@b.c "+Pattern.matches(emailRegexp, "a@b.c"));
    }
}
